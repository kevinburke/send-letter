import argparse
import json
import logging
import os
import subprocess

from jinja2 import Environment, FileSystemLoader
import requests

contact_help = ("The name of file containing email addresses, "
                "separated by newlines")

parser = argparse.ArgumentParser(description='Parse and send an email')
parser.add_argument('--directory', required=True,
                    help='Directory to send out an email for')
parser.add_argument('--go-run', action='store_true', default=False,
                    help="Actually send emails (turn off dry-run mode)")
parser.add_argument('--debug', action='store_true', default=False,
                    help="Debug mode (prints out debugging information)")
parser.add_argument('--no-unsubscribe', action='store_true', default=False,
                    help="Don't add unsubscribe information to email.")
parser.add_argument('contact_file', help=contact_help)

def compile_markdown(directory, output_file):
    markdown_file = "{directory}/letter.md".format(directory=directory)
    subprocess.check_call("markdown {mdfile} > {drctry}/{ofile}".format(
        mdfile=markdown_file, drctry=directory, ofile=output_file), shell=True)

def get_markdown_letter(directory):
    with open(os.path.join(directory, 'letter.md')) as f:
        return f.read().strip()

def get_subject(directory):
    with open(os.path.join(directory, 'subject')) as f:
        return f.read().strip()

def get_leader(directory):
    try:
        with open(os.path.join(directory, 'leader')) as f:
            return f.read().strip()
    except IOError:
        return ""

def get_template(directory, template_base):
    path = os.path.dirname(os.path.realpath(__file__))
    loader = FileSystemLoader('{path}/templates'.format(path=path))
    env = Environment(loader=loader)
    template = env.get_template('email.html')
    return template

def get_contacts(contacts_file):
    to_be_emailed = []

    with open(contacts_file) as f:
        all_lines = f.readlines()

    for line in all_lines:
        if line != '\n' and line[0] != '#':
            to_be_emailed.append(line[:-1])

    return to_be_emailed

def get_config(filename):
    with open(filename) as f:
        return json.load(f)

def get_mailgun_api_key(config):
    return config['providers']['mailgun']['api_key']

def get_mailgun_host(config):
    return config['providers']['mailgun']['host']

def get_letter_instance(directory, template_base):
    """ Retrieve the letter content as HTML """
    with open(os.path.join(directory, template_base)) as the_letter:
        return the_letter.read()

def get_sender(config):
    return config['sender']

def get_footer_html(config):
    return config.get('footer_html')

def get_unsubscribe_link(config):
    return config.get('unsubscribe_link')


def main():

    args = parser.parse_args()

    logger = logging.getLogger('kevin.letter')

    if args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logging.basicConfig()

    template_base = 'letter.html'
    compile_markdown(args.directory, template_base)

    contacts = get_contacts(args.contact_file)
    logger.debug('Contacts are {contacts}'.format(contacts=contacts))

    config = get_config('config.json')
    api_key = get_mailgun_api_key(config)
    mailgun_host = get_mailgun_host(config)
    sender = get_sender(config)
    footer_html = get_footer_html(config)

    markdown_letter = get_markdown_letter(args.directory)
    template = get_template(args.directory, template_base)
    content = get_letter_instance(args.directory, template_base)

    leader = get_leader(args.directory)
    subject = get_subject(args.directory)
    unsubscribe_link = get_unsubscribe_link(config)

    uri = "https://api.mailgun.net/v2/{host}/messages".format(host=mailgun_host)
    auth = ("api", api_key)
    DATA = {
        "from": sender,
        "subject": subject,
        "text": markdown_letter,
    }

    headers = {'User-Agent': 'kevin-letter/0.1'}

    for email_address in contacts:
        edata = DATA.copy()
        edata['to'] = email_address
        html_content = template.render(do_unsubscribe=not args.no_unsubscribe,
                                       unsubscribe_link=unsubscribe_link,
                                       footer_html=footer_html,
                                       leader=leader,
                                       content=content, email=email_address)
        edata['html'] = html_content
        if args.go_run:
            try:
                resp = requests.post(uri, auth=auth, data=edata, headers=headers)
                resp.raise_for_status()
                raw = resp.json()
                logger.info("Sent to {email}. Id is {tid}".format(
                    email=email_address, tid=raw['id']))
            except Exception:
                logger.exception("Sending to " + email_address + " failed.")
        else:
            #logger.debug("Contents are {contents}".format(contents=html_content)
            logger.info("[dry-run] Would have been sent to " + email_address)

if __name__ == "__main__":
    main()

