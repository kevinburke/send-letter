<?php

/**
 * Why PHP? Because you can deploy it to every VPS with minimal configuration.
 */

    function isValidEmail($email){
        return filter_var(filter_var($email, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
    }
    $email = $_REQUEST['e'];
    if (isValidEmail($email)) {
        $text = "Hello,\n" . $email . " wants to unsubscribe from your newsletter.";
        $result = mail('example@example.com', "Unsubscribe from your newsletter", $text);
        if ($result == 1) {
?>
<html>
<body>
<div style="width:300px; margin:40px; font-family:serif; font-size:16px;">
Unsubscribed. Sorry to see you go, hopefully we'll meet again soon!
</div>
</body>
</html>
    <?php } ?>
    <!-- otherwise,  do nothing -->
<?php } else { ?>
<html>
<body>
Nothing to see here.
</body>
</html>
<?php } ?>

