## Send Markdown letters to a group of contacts

This repository contains a Python program that you can use to send
Markdown-formatted emails to contacts specified in a text file.

Campaigns are stored as directories inside this project. Configuration is
stored at the top level in a file called `config.json`.

### Getting Started

Send your first email by following these steps:

1. Copy `config.json.sample` to `config.json`, and populate it with values that
   make sense to you. You will need to sign up for a Mailgun account.

2. Create a directory, and inside it, write an email in Markdown format. Save
   this email as `letter.md`.

3. In that directory, create a file called `subject`. In that file, store the
   subject line of the email.

4. If you would like to customize the lead-in email text, that's visible below
or next to the subject line in email clients, add a file called `leader` in
that directory with the contents you'd like to put there.

5. Create a test contact file and add your email to it. Save this file as
`test_contacts.txt`.

6. Finally, run:

        python send_letter.py --directory my-sample-directory --go-run test_contacts.txt

This should send an email to the email addresses specified in your contact file.

## Usage

```
usage: send_letter.py [-h] --directory DIRECTORY [--go-run] [--debug]
                      [--no-unsubscribe]
                      contact_file

Parse and send an email

positional arguments:
  contact_file          The name of file containing email addresses, separated
                        by newlines

optional arguments:
  -h, --help            show this help message and exit
  --directory DIRECTORY
                        Directory to send out an email for
  --go-run              Actually send emails (turn off dry-run mode)
  --debug               Debug mode (prints out debugging information)
  --no-unsubscribe      Don't add unsubscribe information to email.
```

### The config.json file format

`config.json` accepts the following keys:

- `sender`: The "From" address you'd like to use. Can be a plain email address
or a name and email, e.g. `"Kevin Burke <kev@inburke.com>"`

- `footer_html`: Add custom text here to be inserted in the email footer.
  Alternatively, edit templates/email.html with the content that you want.

- `unsubscribe_link`: Where to send users that would like to unsubscribe.

- `providers`: A list of email providers to use. Currently only `mailgun` is
  supported. Mailgun supports the following keys:

    - `api_key`: Your Mailgun API key.
    - `host`: The email domain to use for sending emails.

A successful `providers` configuration looks like this:

    "providers": {
        "mailgun": {
            "api_key": "key-fooo-foobarbaz-bangbazblahblah",
            "host": "example.com"
        }
    },

### The directory file format

Each email campaign you'd like to send is represented as a directory.

### Ensuring your changes aren't committed to source

You may want to edit your git repository to ensure that your directories aren't
committed to source code. To do this, edit `.git/info/exclude`, and add your
changes there; this is a `.gitignore` file that is local to your own machine.
This way your directories and updates won't be committed to the source history.
You may also want to consider forking this repo, adding changes and
occasionally rebasing with updates from the upstrem repository.


